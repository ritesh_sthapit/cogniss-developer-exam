const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var reverseString = {};
var output = [];
reverseString.emails = [];

jsonfile.readFile(inputFile, function (err, body) {
	output = body.names;
	len = output.length;
	for (i = 0; i < len; i++) {
		reverseString.emails.push(
			output[i].split("").reverse().join("") +
				randomstring.generate(5) +
				"@gmail.com"
		);
	}
	jsonfile.writeFile(outputFile, reverseString, { spaces: 2 }, function (err) {
		console.log("All done");
	});
});
